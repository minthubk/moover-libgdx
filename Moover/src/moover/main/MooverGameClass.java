package moover.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.TimeUtils;

public class MooverGameClass implements ApplicationListener {
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Player player;
	private Player computerPlayer;

	private Vector3 lastTouchPosition;
	private Planet lastPlanetSelected;

	private int TEXTURE_WIDTH = 64;
	private int TEXTURE_HEIGHT = 64;

	private List<Planet> allPlanets;
	private List<Planet> selectedPlanets;
	private List<Plane> planesFlying;
	private List<Plane> planesStarted;

	private long timeSinceLastMove;
	private long speed = 150000000;

	private BitmapFont bitmapFont;

	@Override
	public void create() {
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera(1, h / w);
		camera.setToOrtho(false, 640, 640);
		batch = new SpriteBatch();

		player = new Player("Grzesiek", "player.png", "player_chosen.png",
				"ufo.png", false);
		computerPlayer = new Player("Computer", "AI_1.png", "AI_1_chosen.png",
				"player_plane.png", true);

		allPlanets = new ArrayList<Planet>();
		selectedPlanets = new ArrayList<Planet>();
		planesFlying = new ArrayList<Plane>();
		planesStarted = new ArrayList<Plane>();

		Planet p = new Planet(player, new Point(0, 0), 10);
		allPlanets.add(p);
		p = new Planet(player, new Point(100, 100), 20);
		allPlanets.add(p);
		p = new Planet(player, new Point(300, 200), 30);
		allPlanets.add(p);
		p = new Planet(computerPlayer, new Point(400, 150), 60);
		allPlanets.add(p);
		p = new Planet(computerPlayer, new Point(250, 450), 10);
		allPlanets.add(p);

		bitmapFont = new BitmapFont();
		bitmapFont.setColor(Color.BLACK);

		lastTouchPosition = new Vector3();

	}

	@Override
	public void dispose() {
		batch.dispose();
		player.dispose();
		computerPlayer.dispose();
		bitmapFont.dispose();

	}

	@Override
	public void render() {

		if (TimeUtils.nanoTime() - timeSinceLastMove > speed) {

			enableStartingPlanes();

			for (Planet planet : allPlanets) {
				planet.updatePoints();
			}

			timeSinceLastMove = TimeUtils.nanoTime();
			Planet planet = null;

			if (Gdx.input.isButtonPressed(Buttons.LEFT)) {
				Vector3 vector3 = new Vector3();
				vector3.set(Gdx.input.getX(), Gdx.input.getY(), 0);
				camera.unproject(vector3);

				lastTouchPosition.set(vector3);

				planet = getPlanetByPosition(vector3.x, vector3.y);
				if (planet != null) {
					lastPlanetSelected = planet;
					if (!planet.player.isAI()) {
						selectedPlanets.add(planet);
					}
					planet.selectPlanet();

				} else {
					if (lastPlanetSelected != null
							&& lastPlanetSelected.player.isAI()) {
						lastPlanetSelected.unselectPlanet();
						lastPlanetSelected = null;
					}
				}

			} else {
				if (lastPlanetSelected != null) {
					lastPlanetSelected.unselectPlanet();
				}
				// TODO - niech nie robi tego, je�eli pu�ci�em dotyk -
				// wyzerowa�
				// lastTouchPosition i zrobi� tutaj if-a na nulla
				Planet computerPlanet = getComputerPlanetByPosition(
						lastTouchPosition.x, lastTouchPosition.y);
				if (computerPlanet != null) {

					for (Planet selectedPlanet : selectedPlanets) {

						Plane plane = selectedPlanet.startPlane(computerPlanet);
						if (plane != null) {
							planesStarted.add(plane);
							selectedPlanet.setCanStartPlane(false);
						}

					}
				}
				for (Planet p : selectedPlanets) {
					p.unselectPlanet();
				}
				selectedPlanets.clear();

			}

		}

		for (Plane plane : planesStarted) {
			planesFlying.add(plane);
		}
		planesStarted.clear();

		Iterator<Plane> planesFlyingIt = planesFlying.iterator();
		while (planesFlyingIt.hasNext()) {
			if (planesFlyingIt.next().isArrived()) {
				planesFlyingIt.remove();
			}
		}

		Gdx.gl.glClearColor(0.5f, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		for (Planet p : allPlanets) {
			p.draw(batch);
		}
		for (Plane plane : planesFlying) {
			plane.moveNoDelay();
			plane.draw(batch);
		}

		for (Planet planet : allPlanets) {
			bitmapFont.draw(batch, String.valueOf(planet.getValue()),
					planet.getPosition().x
							+ planet.getPlayer().getTextures()[0].getWidth()
							/ 3, planet.getPosition().y
							+ planet.getPlayer().getTextures()[0].getHeight()
							/ 2);
		}

		for (Plane plane : planesFlying) {
			bitmapFont.draw(batch, String.valueOf(plane.getValue()),
					plane.getPosition().x
							+ plane.getPlayer().getPlaneTexture().getWidth(),
					plane.getPosition().y
							+ plane.getPlayer().getPlaneTexture().getHeight()
							/ 2);
		}

		batch.end();

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {

	}

	public Planet getPlanetByPosition(float x, float y) {

		for (Planet planet : allPlanets) {
			if (isPositionInPlanetPosition(x, y, planet.getPosition())) {
				return planet;
			}
		}
		return null;

	}

	public Planet getComputerPlanetByPosition(float x, float y) {
		for (Planet planet : allPlanets) {
			if (planet.player.isAI()
					&& isPositionInPlanetPosition(x, y, planet.getPosition())) {
				return planet;
			}
		}
		return null;
	}

	public boolean isPositionInPlanetPosition(float x, float y, Point position) {

		if (x >= position.x && x <= position.x + TEXTURE_WIDTH
				&& y >= position.y && y <= position.y + TEXTURE_HEIGHT)
			return true;
		return false;
	}

	public void enableStartingPlanes() {
		for (Planet p : allPlanets) {
			p.setCanStartPlane(true);
		}
	}

}
