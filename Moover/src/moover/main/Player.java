package moover.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Disposable;

public class Player implements Disposable {

	private Texture textures[];
	private String name;
	private boolean isAI;
	private Texture planeTexture;

	public Player(String name, String textureName, String textureName2,
			String planeTextureName, boolean isComputer) {
		this(name, textureName, textureName2, planeTextureName);
		isAI = isComputer;
	}

	public Player(String name, String textureName, String textureName2,
			String planeTextureName) {
		this.name = name;
		textures = new Texture[2];
		textures[0] = new Texture(Gdx.files.internal(textureName));
		textures[1] = new Texture(Gdx.files.internal(textureName2));
		planeTexture = new Texture(Gdx.files.internal(planeTextureName));
	}

	public Texture[] getTextures() {
		return textures;
	}

	public void setTextures(Texture[] textures) {
		this.textures = textures;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAI() {
		return isAI;
	}

	public void setAI(boolean isAI) {
		this.isAI = isAI;
	}

	@Override
	public void dispose() {
		textures[0].dispose();
		textures[1].dispose();
	}

	@Override
	public String toString() {
		return "Player[" + "name=" + name + "]";
	}

	public Texture getPlaneTexture() {
		return planeTexture;
	}

	public void setPlaneTexture(Texture planeTexture) {
		this.planeTexture = planeTexture;
	}

}
