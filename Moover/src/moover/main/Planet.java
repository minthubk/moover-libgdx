package moover.main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.TimeUtils;

import moover.main.Point;

public class Planet extends GameObject {

	private Point fontPosition;
	private boolean canStartPlane;

	public Planet(Player player, Point position, int value) {
		super(player, position, value);
		fontPosition = new Point();
		canStartPlane = true;
	}

	public void changeTexture() {

		if (currentTexture == 1)
			currentTexture = 0;
		else if (currentTexture == 0)
			currentTexture = 1;
	}

	public void selectPlanet() {
		currentTexture = 1;
	}

	public void unselectPlanet() {
		currentTexture = 0;
	}

	public Point getFontPosition() {
		return fontPosition;
	}

	public void setFontPosition(Point fontPosition) {
		this.fontPosition = fontPosition;
	}

	public boolean isCanStartPlane() {
		return canStartPlane;
	}

	public void setCanStartPlane(boolean canStartPlane) {
		this.canStartPlane = canStartPlane;
	}

	@Override
	public String toString() {
		return "Planet[player=" + player + ", position=" + position
				+ ", radius=" + radius + ", value=" + value + "]";
	}

	public Plane startPlane(Planet destination) {
		if (canStartPlane == false) {
			return null;
		}
		Point startingPosition = new Point();
		// TODO change starting position of the plane
		startingPosition.setLocation(position);
		Point speed = new Point(1, 1);
		value /= 2;
		Plane result = new Plane(player, startingPosition, value, speed,
				destination);
		Gdx.app.log("Plane", "Plane - started :" + result);
		return result;
	}

	public void updatePoints() {

		if (TimeUtils.nanoTime() - timeSinceLastAction > TIME_TO_ACTION) {
			value++;
			timeSinceLastAction = TimeUtils.nanoTime();
		}

	}

}
