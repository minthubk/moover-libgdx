package moover.main;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

import moover.main.Point;

public abstract class GameObject implements Disposable {

	protected static int TIME_TO_ACTION = 600000000;

	protected Player player;
	protected Point position;
	protected int radius;
	protected int value;
	protected long timeSinceLastAction;
	protected int currentTexture;

	public GameObject(Player player, Point position, int value) {
		this.player = player;
		this.position = new Point();
		this.position.setLocation(position);
		this.value = value;
	}

	public static int getTIME_TO_ACTION() {
		return TIME_TO_ACTION;
	}

	public static void setTIME_TO_ACTION(int tIME_TO_ACTION) {
		TIME_TO_ACTION = tIME_TO_ACTION;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public long getTimeSinceLastAction() {
		return timeSinceLastAction;
	}

	public void setTimeSinceLastAction(long timeSinceLastAction) {
		this.timeSinceLastAction = timeSinceLastAction;
	}

	public int getCurrentTexture() {
		return currentTexture;
	}

	public void setCurrentTexture(int currentTexture) {
		this.currentTexture = currentTexture;
	}

	public void draw(SpriteBatch batch) {
		batch.draw(player.getTextures()[currentTexture], position.x, position.y);
	}

	@Override
	public void dispose() {
		player.dispose();

	}

}
