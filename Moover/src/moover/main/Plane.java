package moover.main;

import moover.main.Point;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.TimeUtils;

public class Plane extends GameObject {

	private Point speed;
	private Planet destinationPlanet;
	private Point fontPosition;
	private boolean arrived;

	public Plane(Player player, Point position, int value, Point spd,
			Planet destination) {
		super(player, position, value);
		destinationPlanet = destination;
		speed = new Point(spd);
		fontPosition = new Point();
		fontPosition.setLocation(position);
		arrived = false;
	}

	public Point getSpeed() {
		return speed;
	}

	public void setSpeed(Point speed) {
		this.speed = speed;
	}

	public Planet getDestinationPlanet() {
		return destinationPlanet;
	}

	public void setDestinationPlanet(Planet destinationPlanet) {
		this.destinationPlanet = destinationPlanet;
	}

	public void move() {
		if (TimeUtils.nanoTime() - timeSinceLastAction > TIME_TO_ACTION) {
			timeSinceLastAction = TimeUtils.nanoTime();
			position.translate(speed.x, speed.y);
		}
	}

	@Override
	public String toString() {
		return "Plane[destinationPlanet=" + destinationPlanet + ", player="
				+ player + ", position=" + position + ", value=" + value + "]";
	}

	@Override
	public void draw(SpriteBatch batch) {
		batch.draw(player.getPlaneTexture(), position.x, position.y);
	}

	public void moveNoDelay() {
		if (!arrived) {

			if (position.equals(destinationPlanet.position)) {

				if (player == destinationPlanet.getPlayer()) {
					destinationPlanet.value += value;
				} else {
					destinationPlanet.value -= value;
					if (destinationPlanet.value < 0) {
						destinationPlanet.player = player;
					}
					destinationPlanet.value = Math.abs(destinationPlanet.value);
				}

				arrived = true;
			}
			if (position.x < destinationPlanet.position.x)
				position.x += speed.x;
			else
				position.x -= speed.x;
			if (position.y < destinationPlanet.position.y)
				position.y += speed.y;
			else
				position.y -= speed.y;
		}
	}

	public boolean isArrived() {
		return arrived;
	}

	public void setArrived(boolean arrived) {
		this.arrived = arrived;
	}

}
